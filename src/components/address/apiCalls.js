import axios from 'axios'

export const apis = {
    lookup: function (url, success_, error_) {
        axios.get(url)
            .then(response => {
                success_(response)
            })
            .catch(error => {
                console.log('error', error.status)
                error_(error)
            })
    },
    autocomplete: function (address) {
        
    },
    getAddressesAtPostcode: function () {
        
    }
}