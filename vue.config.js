const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
    configureWebpack: {
        externals: {
            //axios: 'axios',
            // moment: 'moment'
        },
        plugins: [
            //new BundleAnalyzerPlugin(),
            new webpack.ProvidePlugin({
                eventHub: 'vue-event-hub',
                $eventHub: 'vue-event-hub',
                moment: 'moment',
                nouislider: 'nouislider'
            }),
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
        ]
    }
}