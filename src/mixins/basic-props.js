export const basic_props = {
    props: {
        /* inputname
         * @string
         * sets the input id/name
         * used when emitting the input's value
        */
        inputname: {
            type: String,
            required: true
        },

        /* label
         * @string
         * sets <label> element copy
         * is used as a default placeholder value
        */
        label:  {
            type: String,
            required: true
        },

        /* autocomplete
         * @string
         * used to specifiy which browser autocomplete value to use
        */
        autocomplete:  {
            type: [String, Object],
            required: false
        },

        /* value
         * @object ({ value: 1 })
         * used to prepopulate the input value
        */
        value: Object,

        /* required
         * @bool
         * used to check if input required validation should be used
        */
        required: Boolean,
        
        /* helpText
         * @string
         * used to include any help text required for the element
        */
        helpText: String,
        
        /* sectionID
         * @number
         * used to see if the input should be validated when 'validate' event if fired
         * e.g. if sectionID = 2 and eventID = 2 then we'll validate the input
        */
        sectionID: Number,

        /* iconClass
         * @string
         * used to pass a class to the .input-wrapper class which can be used to display an icon
        */
        iconClass: String,

        /* softValidate
            * @boolean
            * used to enable 'soft' validation on input keydown event
        */
        softValidate: Boolean,
        /*disabled: {
            type: Boolean,
            default: false,
        },*/
        isPrepop : {
            type: Boolean,
            default: false
        },
        useWatch: Boolean
    }
}