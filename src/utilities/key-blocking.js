export const block = {
    letters: function (event) {
        let e = event || window.event;
        let charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
        let charStr = String.fromCharCode(charCode);

        if (charCode == 32 || /[a-zA-Z]/.test(charStr))
            if (charCode != 97 && charCode != 98 && charCode != 99 && charCode != 100 && charCode != 101 && charCode != 102 && charCode != 103 && charCode != 104 && charCode != 105)
                e.preventDefault()
    },
    numbers: function (event) {
        let e = event || window.event;
        const charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
        const charStr = String.fromCharCode(charCode);
        if (/\d/.test(charStr) || charCode === 97 || charCode === 98 || charCode === 99 || charCode === 100 || charCode === 101 || charCode === 102 || charCode === 103 || charCode === 104 || charCode === 105) {
          e.preventDefault()
        }
    },
    decimals: function (event) {
        let e = event || window.event;
        let charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
        if (charCode === 190) {
            e.preventDefault()
        }
    },
    spaces: function (event) {
        let e = event || window.event;
        let charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
        if (charCode === 32) {
            e.preventDefault()
        }
    }
}