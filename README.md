# @thinknpm/vue-form-ui

A Collection of Vue.js Form Components

:abc: Text input (only allows letters)

:1234: Number input (only allows numbers)

:date: Date input (3 inputs split DD/MM/YYYY - only allows valid dates and has optional min/max age validation)

:e-mail: Email input (valid emails only)

:iphone: Phone input (UK mobile or home)

:radio_button: Radio buttons input (fieldset)

:ballot_box_with_check: Checkbox buttons input (fieldset)

:arrow_down_small: Select input

:pound: Currency input

## Usage
#### Install the package from npm:
```
npm install @thinknpm/vue-form-ui --save
```

#### Include the components you wish to use
```javascript
// import form components
import TextInput from '@thinknpm/vue-form-ui/src/components/textinput.vue'
import EmailInput from '@thinknpm/vue-form-ui/src/components/emailinput.vue'

// vue component
export default {
  name: 'Form',
  components: {
      'text-input': TextInput,
      'email-input': EmailInput,
      // lazyload/code split the component
      // components can be grouped using the same 'webpackChunkName'
      'phone-input': () => import(/* webpackChunkName: "code-split-1" */ '@thinknpm/vue-form-ui/src/components/phoneinput.vue')
  }
}
```
##### Add inputs to your template
```html
<text-input inputname="firstName" label="First Name" :required="true" />
<email-input inputname="email" label="Email" :required="true" />
<phone-input inputname="mobile" label="Mobile number" type="mobile" />
```

## All input types have the following props:
**These are imported via a Vue mixin:** ```import { basic_props } from '../mixins/basic-props.js'```

### ```inputname (string) required```
name given to the input
### ```label (string) required``` 
string used to fill question and placerholder text
### ```value (object)```
used to prepopulate input value from an object e.g ```{ value: 'prepopulated value' }```
### ```required (boolean)```
basic validation - is the field required
### ```helpText (string)```
string which can be used to display a help message
### ```sectionID (number)```
number which is used when you want to validation form sections without triggering inputs in different 'sections'
### ```customErrors (object)```
**This is not included via mixin because defaults are different for input types**

Object containing custom error messages for each rule e.g. ```{ required: 'Please provide your name' }```

## There is also additional props for some input types:
### ```minLength (string)```
**components: ```TextInput, EmailInput, PhoneInput```**

Set the minimum length of input value

### ```maxLength (string)```
**components: ```TextInput, EmailInput, PhoneInput```**

Set the maximum length of input value

### ```minAge (number)```
**components: ```DateInput```**

Set the minimum age for date validation

### ```maxAge (number)```
**components: ```DateInput```**

Set the maximum age for date validation

### ```type (string)```
**components: ```PhoneInput```**

Set phone input type to either 'mobile' or 'home'

### ```options (array)```
**components: ```ButtonsRadio, ButtonsCheckbox, SelectInput```**

An array of objects used to provide options for the buttons e.g. ```[{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }]```
