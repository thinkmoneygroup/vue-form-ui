export const validation = {
  data: function () {
    return {
      // holds the input's value
      inputValue: '',
      // holds the valid status
      isValid: null,
      // holds the validation error message (if applicable)
      validationMessage: null,
      // used to add/remove focused class from input
      focused: false,
      // used to disable when prepopulating
      disabled : false
    }
  },
  methods: {
    // checks to see if validation should be fire
    /*
      this is useful when working with 'sectioned' forms
      where for example you wish to validate inputs within a certain section
      but not all inputs available on the page
    */
    mixin_shouldValidateBool: function (eventID, sectionID) {
      // if validating by section
      if (sectionID !== undefined && eventID !== undefined) {
          // if the id provided matches section ID then validate
          if (eventID === sectionID) {
            return true
          }
      } else {
        // if not validating by section
        return true
      }
    },

    // set isValid and validationMessage variable values
    /*
      this is passed an array of validation fuction results
      (either a true bool or an object containing message value)
    */
    mixin_setValidationStatus: function (resultsArray, softValidate, isDate) {
      // check validation result
      var validationPass = true;

      resultsArray.forEach(function(result){
        if (result != true) {
          validationPass = false
        }
      })

      if (validationPass) {
        // if all array items equal true
        // validation passes
        this.isValid = true
        this.validationMessage = ""
      } else if (!softValidate) {
        // if there is a validation false boolean
        // loop through and return the first error found
        for (var i = 0, len = resultsArray.length; i < len; i++) {
          if (resultsArray[i] != true) {
            // set valid variable to false
            this.isValid = false
            // set the error message to the message provided by validation
            this.validationMessage = resultsArray[i].message
            // used to return the first date error
            // doesnt appear to be needed for other input types
            if (isDate !== undefined && isDate === true) {
              return
            }
          }
        }
      } else {
        this.isValid = null
      }
    },

    // pass input data to parent
    /* 
      passes the input component's value, name and valid status to the parent component
      data is sent via a specific emit event, either 'validated', 'prepopulated' or 'addressConfirmed' these can be listened to by the parent and data handled
    */
    mixin_emitValueObject: function (type, value, name, valid, error_message) {
      let typeNotSet = (type !== 'prepopulated' && type !== 'addressConfirmed')
      // default the type to 'validated' if not using prepopulated event type
      // let emitType = typeNotSet ? 'validated' : type
      // emit the event value to parent components
      this.$emit('validated', { 
        'value': value, 
        'name': name, 
        'isValid': valid,
        'error_message': error_message
      })
    }
  }
}