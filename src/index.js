const ButtonsRadio = require('./components/buttons-radio.vue').default;
const ButtonsCheckbox  = require('./components/buttons-checkbox.vue').default;
const DateInput  = require('./components/dateinput.vue').default;
const EmailInput  = require('./components/emailinput.vue').default;
const InputWithButton  = require('./components/inputwithbutton.vue').default;
const PhoneInput  = require('./components/phoneinput.vue').default;
const PhoneInputWithButton  = require('./components/phoneinput-w-button.vue').default;
const TextInput  = require('./components/textinput.vue').default;
const NumberInput  = require('./components/numberinput.vue').default;
const SelectInput  = require('./components/selectinput.vue').default;
const CurrencyInput  = require('./components/currencyinput.vue').default;
const CheckboxInput  = require('./components/checkboxinput.vue').default;
const AddressBlock  = require('./components/addressblock.vue').default;
const AddressAutoCompleteBlock  = require('./components/autocomplete-address.vue').default;
const MonthYearInput  = require('./components/month-year-input.vue').default;
const TimeSpentInput  = require('./components/time-years-months.vue').default;
const SortCodeInput  = require('./components/sortcode-input.vue').default;

const FormComponents = {
    TextInput,
    NumberInput,
    SelectInput,
    ButtonsRadio,
    ButtonsCheckbox,
    DateInput,
    PhoneInput,
    PhoneInputWithButton,
    EmailInput,
    CurrencyInput,
    InputWithButton,
    CheckboxInput,
    MonthYearInput,
    AddressBlock,
    AddressAutoCompleteBlock,
    TimeSpentInput,
    SortCodeInput
}

//export default FormComponents
module.exports = FormComponents