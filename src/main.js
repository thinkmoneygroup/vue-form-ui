import Vue from 'vue'
import App from './App.vue'

import EventHub from 'vue-event-hub'
Vue.use(EventHub)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
