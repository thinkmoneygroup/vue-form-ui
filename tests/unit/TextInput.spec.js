// import shallowMount - to render components
import { shallowMount } from '@vue/test-utils'

// import Vue
import Vue from 'vue'

// import vue-event-hub - used by components to pass data to parent 
import EventHub from 'vue-event-hub'
Vue.use(EventHub)

// import our TextInput component
import TextInput from '@/components/textinput.vue'


// descripe the series of tests we're running
// here we descript TextInput because we're testing our TextInput component
describe("TextInput", () => {
  // setup dummy variable for component and input (<input> HTML elm)
  let component
  let input

  // before each test is ran
  // this is used to mount the component,
  // setup its props,
  // and update input variable with the <input> element rendered
  beforeEach(() => {
    // create/mount the component
    component = shallowMount (TextInput, {
      // set component props
      propsData: {
        label: 'test input',
        inputname: 'test_input_name',
        autocomplete: 'fname',
        minLength: 2,
        maxLength: 10,
        helpText: 'Some example help text',
        required: true
      }
    });
    // get the component input
    input = component.find('input')
  });

  // run some basic tests to check props passed and html rendered as expected
  test('basic element checks - label', () => {
    expect(component.html()).toContain('<label for="test_input_name">test input</label>')
  });
  test('basic element checks - input', () => {
    expect(component.html()).toContain(
      '<input type="text" id="test_input_name" name="test_input_name" autocomplete="fname" placeholder="test input" minlength="2" maxlength="10" class="">'
    );
  });
  test('basic element checks - help text', () => {
    expect(component.html()).toContain(
      '<span class="info-message">Some example help text</span>'
    );
  });
  test('basic element checks - error message', () => {
    expect(component.html()).toContain(
      '<p class="error-message"></p>'
    );
  });
  test('basic element checks - custom error message', () => {
    component.setProps({ customErrors: { required: 'This is a custom required message'} })
    input.trigger('blur')
    expect(component.html()).toContain(
      '<p class="error-message">This is a custom required message</p>'
    );
  });
  test('basic element checks - sets data() value', () => {
    const testValue = 'testing a string'
    /*component.setData({inputValue: testValue});*/
    input.setValue(testValue)
    // trigger blur (for validation)
    input.trigger('blur')

    const data = component.vm.inputValue
    
    expect(data).toEqual(testValue);    
  });  
  test('basic element checks - focus() should null valid status', () => {
    // set value to 
    input.setValue("")
    // trigger blur (for validation)
    input.trigger('blur')
    // check error class applied
    expect(input.classes('error')).toBe(true)
    // null the validation with focus()
    input.trigger('focus')
    // check error class removed
    expect(input.classes('error')).toBe(false)
  });


  // test validation rules
  ////////////////////////
  // test valid required validation
  test('validation - required - valid', () => {
    // set value to 
    input.setValue("testing")
    // trigger blur (for validation)
    input.trigger('blur')
    // run the test
    const data = component.vm.inputValue
    expect(data).toEqual("testing"); 
    expect(component).toMatchSnapshot()
  });
  // invalid required
  test('validation - required - invalid', () => {
    // tirgger blur (for validation)
    input.trigger('blur')
    // run the test
    expect(component).toMatchSnapshot()
  });
  // invalid minlength
  test('validation - minlength - invalid', () => {
    // set value to 
    input.setValue("t")
    // trigger blur (for validation)
    input.trigger('blur')
    // run the test
    expect(component).toMatchSnapshot()
  });
  // invalid maxlength
  test('validation - maxlength - invalid', () => {
    // set value to 
    input.setValue("testing a long string")
    // trigger blur (for validation)
    input.trigger('blur')
    // run the test
    expect(component).toMatchSnapshot()
  });
  // invalid textonly
  test('validation - text only - invalid', () => {
    component.setProps({ textOnly: true })

    // set value to 
    input.setValue("test123")
    // trigger blur (for validation)
    input.trigger('blur')
    // run the test
    expect(component).toMatchSnapshot()
  });
});

